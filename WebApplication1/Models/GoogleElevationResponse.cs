﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TimeAndElevation.Models
{
    public class GoogleElevationResponse
    {
        public GoogleElevationResults[] Results { get; set; }
        //{
        //   "results" : [
        //      {
        //         "elevation" : 1608.637939453125,
        //         "location" : {
        //            "lat" : 39.73915360,
        //            "lng" : -104.98470340
        //         },
        //         "resolution" : 4.771975994110107
        //      }
        //   ],
        //   "status" : "OK"
        //}   
        public string Status { get; set; }
    }

    public class GoogleElevationResults
    {
        public decimal Elevation { get; set; }
    }
}
