﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TimeAndElevation.Models;

namespace TimeAndElevation.Controllers
{
    [Route("api/[controller]")]
    public class DataController : Controller
    {
        private readonly IOptions<Settings> config;

        public DataController(IOptions<Settings> config)
        {
            this.config = config;
        }

        [HttpGet("{zip}")]
        public async Task<string> Get(string zip)
        {
            try
            {
                var weather = await GetWeather(zip);
                var timeZone = await GetTimeZone(weather.Coord.Lat, weather.Coord.Lon);
                var elevation = await GetElevation(weather.Coord.Lat, weather.Coord.Lon);

                return $"At the location {weather.Name}, the temperature is {weather.Main.Temp}F, the timezone is {timeZone.timeZoneName}, and the elevation is {elevation.Results.First().Elevation.ToString("N2")}m.";
            }
            catch (HttpRequestException httpRequestException)
            {
                return "Failed to get the data.";
            }
        }

        private async Task<OpenWeatherResponse> GetWeather(string zip)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.openweathermap.org");
                var response = await client.GetAsync($"/data/2.5/weather?zip={zip}&appid={config.Value.OpenWeatherMapApiKey}&units=imperial");
                response.EnsureSuccessStatusCode();

                var stringResult = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<OpenWeatherResponse>(stringResult);
            }
        }

        private async Task<GoogleTimeZoneResponse> GetTimeZone(decimal latitude, decimal longitude)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://maps.googleapis.com");
                var response = await client.GetAsync($"/maps/api/timezone/json?location={latitude},{longitude}&timestamp={DateTime.Now.ToTimestamp()}&key={config.Value.GoogleApiKey}");
                response.EnsureSuccessStatusCode();

                var stringResult = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<GoogleTimeZoneResponse>(stringResult);
            }
        }

        private async Task<GoogleElevationResponse> GetElevation(decimal latitude, decimal longitude)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://maps.googleapis.com");
                var response = await client.GetAsync($"/maps/api/elevation/json?locations={latitude},{longitude}&key={config.Value.GoogleApiKey}");
                response.EnsureSuccessStatusCode();

                var stringResult = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<GoogleElevationResponse>(stringResult);
            }
        }
    }
}
