import * as React from 'react';
import { RouteComponentProps } from 'react-router';

interface HomeState {
    forecast: string;
    zip: string;
}

export class Home extends React.Component<RouteComponentProps<{}>, HomeState> {
    constructor() {
        super();
        this.state = { forecast: '', zip: '' };
    }

    public render() {
        return <div>
            <h1>Weather</h1>

            <p>Current weather: <strong>{this.state.forecast}</strong></p>

            <input type="text" value={this.state.zip} onChange={(e) => this.handleChange(e.target.value)} />
            <button onClick={() => { this.get(this.state.zip) }}>Submit</button>
        </div>;
    }

    handleChange(value: string) {
        this.setState({
            zip: value
        });
    }
    get(zip: string) {
        fetch('api/data/' + zip)
            .then(response => response.text())
            .then(data => {
                this.setState({ forecast: data, zip: zip });
            });
    }
}

interface WeatherForecast {
    data: string;
}
