﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TimeAndElevation.Models
{
    public class Settings
    {
        public string OpenWeatherMapApiKey { get; set; }
        public string GoogleApiKey { get; set; }
    }
}
